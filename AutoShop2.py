
class Car:
    def __init__(self, mark, color, year):
        self.mark = mark
        self.color = color
        self.year = year
    def __repr__(self):
        return f'"{self.mark}, {self.color}, {self.year}"'

class AutoShop:
    def __init__(self, car_list):
        self.car_list = car_list

    def sell_car(self):
        return self.car_list.pop()
    def __repr__(self):
        return self.car_list


cars = []
for i in range(1):
    cars.append(Car('Mazda', 'black', '2020'))
    cars.append(Car('Ford', 'blue', '2021'))
    cars.append(Car('BMW', 'red', '2022'))

my_autoshop = AutoShop(cars)

print('Автомобили перед продажей: ', my_autoshop.car_list)
my_autoshop.sell_car()
print('Автомобили в наличии: ', my_autoshop.car_list)