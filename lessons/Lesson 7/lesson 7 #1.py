# Задание 1, урок 7

a = [3, 10, 5, 2]

print("Минимальное:", min(a))
print("Максимальное:", max(a))
print("Сумма чисел", sum(a))

import numpy

print("Среднее арифметическое", numpy.mean(a))