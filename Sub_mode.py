class Editor:
    def edit_document(self):
        print('Редактирование недоступно для бесплатной версии')

    def view_document(self):
        print('Просмотр документа')


class ProEditor(Editor):
    def edit_document(self):
       if input('Введите ключ: 123 ') == '123':
           print('Теперь вы можете редактировать, спасибо за подписку!')
       else:
           super().edit_document()
p = ProEditor()
p.view_document()
p.edit_document()